
import numpy as np
import pandas as pd
from enum import Enum
import sys
import math

class rlalgorithm:
    def __init__(self, actions, terminalState, deathStates, learning_rate=0.03, reward_decay=0.9, e_greedy=0.1):
        self.actions = actions              # [1, 2, 3, 4] for [up down left right]
        self.lr = learning_rate

        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.precision = 0.00002

        self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.q_table = self.q_table.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.q_table.columns,
                    name=terminalState,
                )
            )
        for ds in deathStates:
            self.q_table = self.q_table.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.q_table.columns,
                    name=ds,
                )
            )
        self.action_ocurrences = [0, 0, 0, 0]
        self.display_name="Q Learning Learning"

    '''Choose the next action to take given the observed state using an epsilon greedy policy'''
    def choose_action(self, observation):
        # Observation is the state (4 coordinates)
        # Add state to Q Table
        self.check_state_exist(observation)

        # Get the current cell of the policy grid
        actionValueCell = self.q_table.loc[observation,:]
        randomValue = np.random.uniform() * 100
        normalizedEpsilon = self.epsilon * 100

        maxValue = actionValueCell.max()
        # We are choosing an action that is NOT part of the optimal policy set
        if (randomValue < normalizedEpsilon):

            # Determine which actions are not part of the maximum/optimal choice
            nonMaxIndices = []
            for index, value in actionValueCell.items():
                if (self.precision < abs(maxValue - value)):
                    nonMaxIndices.append(index)

            # If all action-value pairs for this state are equal, then just choose randomly amoung 4 directions
            if (len(nonMaxIndices) == 0):
                nextAction = math.floor(np.random.uniform() * 4)
                # TODO: maybe avoid hard-coding 3
                if (nextAction > 3):
                    nextAction = 3
            else:
                incrementSize = normalizedEpsilon / len(nonMaxIndices) 
                action_idx = math.floor(randomValue / incrementSize)

                if (action_idx >= len(nonMaxIndices)):
                    print("Error: In the wrong place 1")
                    action_idx = len(nonMaxIndices) - 1

                nextAction = nonMaxIndices[action_idx]
        else:
            # Find best action and its index
            maxIndices = []
            for index, value in actionValueCell.items():
                if (self.precision >= abs(maxValue - value)):
                    maxIndices.append(index)
            # print("Max Index array size: ")
            # print(len(maxIndices))
            
            # There are multiple max actions
            incrementSize = (100 - normalizedEpsilon) / len(maxIndices)
            action_idx = math.floor((randomValue - normalizedEpsilon) / incrementSize)

            if (action_idx >= len(maxIndices)):
                print("Error: In the wrong place 2")
                action_idx = len(maxIndices) - 1

            nextAction = maxIndices[action_idx]

        # print("Action")
        # print(nextAction)
        self.action_ocurrences[nextAction] += 1
        # print("Action Occurrences: ")
        # print(self.action_ocurrences)
        # print("Q Table: ")
        # print(self.q_table)
        return nextAction

    def learn(self, env, currentState, currentAction, aheadState, reward):
        self.check_state_exist(currentState)
        self.check_state_exist(aheadState)
        
        actionValueCell = self.q_table.loc[aheadState,:]
        maximum = actionValueCell.max()

        self.q_table.loc[currentState,currentAction] = self.q_table.loc[currentState,currentAction] \
                                                    + self.lr*(reward + (self.gamma*maximum) - self.q_table.loc[currentState,currentAction])
    
    # '''States are dynamically added to the Q(S,A) table as they are encountered'''
    def check_state_exist(self, state):
        if state not in self.q_table.index:
            # append new state to q table
            self.q_table = self.q_table.append(
                pd.Series(
                    [-0.25]*len(self.actions),
                    index=self.q_table.columns,
                    name=state,
                )
            )